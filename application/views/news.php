<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<html>
<head>
	<title>News Search</title>
	<?php include APPPATH."/views/template/plugins_and_stylesheets.php"; ?>
    <script src="<?php echo base_url();?>/assets/js/news_page.js"></script>
</head>

<body>
	<div class="container">
		<div class="row content">
			<div class="col-sm-12">
                <h1>News API Test</h1>
				<div class="row">
					<div class="col-sm-6">
						<div class="input-group">
							<input type="text" class="form-control" id="input-api-key" placeholder="API Key eg.saf15352gdg">
							<span class="input-group-btn">
								<button id="button-show-table" class="btn btn-default" type="button">Show Table</button>
							</span>
						</div>
					</div>
					<div class="col-sm-6">
						<p id="error-message" class="error-message"></p>
					</div>
				</div>
                <div class="loader"></div>
			</div>
		</div>
		<div class="row content">
			<div class="col-sm-12">
				<table id="table-articles" class="table table-striped" style="display: none">
                    <thead>
						<tr>
							<th>Pic</th>
							<th class="th-clickable" onClick="sortArticlesByKey(SORT_TITLE)">Title</th>
							<th class="th-clickable" onClick="sortArticlesByKey(SORT_DESCRIPTION)">Description</th>
							<th class="th-clickable" onClick="sortArticlesByKey(SORT_AUTHOR)">Author</th>
							<th class="th-clickable" onClick="sortArticlesByKey(SORT_URL)">Url</th>
							<th class="th-clickable" onClick="sortArticlesByKey(SORT_PUBLISHED)">Published</th>
						</tr>
					</thead>
				</table>
                <nav aria-label="Page navigation" id="pagination" style="display: none">
                </nav>
			</div>
		</div>
	</div>
</body>
</html>
