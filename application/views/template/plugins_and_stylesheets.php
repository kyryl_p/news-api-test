<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Bootstrap -->
<link href="<?php echo base_url();?>/assets/css/bootstrap.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>/assets/js/moment.min.js"></script>
<!-- Font Awesome -->
<link href="<?php echo base_url();?>/assets/css/font-awesome.min.css" rel="stylesheet" >
<!-- Our Style Override -->
<link href="<?php echo base_url();?>/assets/css/style.css" rel="stylesheet">
