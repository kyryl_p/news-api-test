var articles;
var page = 1;
var articlesPerPage = 3;

const SORT_TITLE = 'title';
const SORT_AUTHOR = 'author';
const SORT_PUBLISHED = 'publishedAt';
const SORT_DESCRIPTION = 'description';
const SORT_URL = 'url';


$(document).ready(function(){
    $('#button-show-table').click(function(){
        $('#error-message').html("");
        var apiKey = $('#input-api-key').val();
        getNews(apiKey);
    })
})

function getNews(apiKey) {
    // show loader
    $(".loader").show();

    var url = "https://newsapi.org/v1/articles?source=abc-news-au&apiKey="+apiKey;
    $.ajax({
        url: url,
        type: 'GET',
        success: function(response) {
            articles = response['articles'];

            // replace nulls with "Unknown"
            $.each(articles, function (index, article) {
                $.each(article, function (key, value) {
                    if (value==null) {
                        article[key] = "Unknown";
                    }
                })
            })
            drawTable();
        },
        error: function() {
            $('#error-message').html("Error: Invalid API Key");
            articles = null;
            drawTable();

            // hide loader
            $(".loader").hide();
        }
    })
}

function sortArticlesByKey(key) {
    var sortCount = 0;
    var direction = 1;
    var tries = 0;

    // do until items are sorted but don't loop more than 2 times
    while (sortCount < 1 && tries < 2) {

        // sorting items by key
        articles.sort(function(a,b) {
            var x = a[key].toLowerCase(); var y = b[key].toLowerCase();
            var result = ((x < y) ? -1 : ((x > y) ? 1 : 0));
            if (result > 0) {
                sortCount++;
            }
            return result * direction;
        });

        // if nothing was sorted, we can reverse the direction of the sort
        if (sortCount < 1) {
            direction = -1;
        }
        tries++;
    }

    drawTable();
}

function drawTable() {

    // show loader
    $(".loader").show();

    // make sure table is hidden
    $('#table-articles').hide();
    // remove tbody if it already exists
    if ($('#tbody-articles')!=null) {
        $('#tbody-articles').remove();
    }

    // make sure pagination is hidden
    $('#pagination').hide();
    // remove ul if it already exists
    if ($('#pagination-ul')!=null) {
        $('#pagination-ul').remove();
    }

    if (articles != null && articles.length > 0) {
        // draw new tbody
        var tbody = $('<tbody />',{id: "tbody-articles"});
        $('#table-articles').append(tbody);

        // add rows to tbody
        var lastIndex = articles.length;
        if (lastIndex > page * articlesPerPage) {
            lastIndex = page * articlesPerPage;
        }

        for (var i = page * articlesPerPage - articlesPerPage; i < lastIndex; i++ ) {
            drawRow(tbody, articles[i]);
        }

        // draw new pagination-ul
        var ul = $('<ul/>',{id: "pagination-ul", class: "pagination right"});
        $('#pagination').append(ul);


        // add back arrow to pagination
        var li = $('<li />');
        ul.append(li);
        li.html("<a><span aria-hidden='true'>&laquo;</span></a>");
        if (page-1<1) {
            li.attr("class","disabled");
        } else {
            li.attr("onClick","changePage("+(page-1)+")");
        }

        // add buttons to the pagination
        var totalPages = Math.ceil(articles.length / articlesPerPage);
        for (var pageNumber = 1; pageNumber <= totalPages; pageNumber++) {
            var li = $('<li />',{onClick: "changePage("+pageNumber+")"});
            ul.append(li);
            li.html("<a>"+pageNumber+"</a>");
            if (page == pageNumber) {
                li.attr("class","active");
            }
        }

        // add forward arrow to pagination
        var li = $('<li />');
        ul.append(li);
        li.html("<a><span aria-hidden='true'>&raquo;</span></a>");
        if (page+1>totalPages) {
            li.attr("class","disabled");
        } else {
            li.attr("onClick","changePage("+(page+1)+")");
        }

        // hide loader
        $(".loader").hide();

        // show the table
        $('#table-articles').show();

        //show pagination
        $('#pagination').show();
    }
}

function drawRow(tbody, article) {

    // make a new row
    var tr = $('<tr />');
    tbody.append(tr);

    // create a cell for an image
    var imgTd = $('<td />',{class: "news-img"});
    imgTd.append($('<img/>',{src: article['urlToImage']}));


    // format the author name
    var author = "";
    if (article['author'] != "Unknown") {
        var authorSubStr = article['author'].split("/")[4];
        for(var i = 0; i < authorSubStr.split("-").length; i++) {
            var name = authorSubStr.split("-")[i];
            author += name.charAt(0).toUpperCase() + name.slice(1)+" ";
        }
    } else {
        author = "Unknown";
    }

    // format date published
    var datePublished = "<p>"+moment(article['publishedAt']).format('Do MMMM Y')+"</p>"+"<p>"+moment(article['publishedAt']).format('h:mm a')+"</p>";

    // draw all the cells
    tr.append(imgTd);
    tr.append($("<td>"+article['title']+"</td>"));
    tr.append($("<td>"+article['description']+"</td>"));
    tr.append($("<td>"+author+"</td>"));
    tr.append($("<td>"+article['url']+"</td>"));
    tr.append($("<td>"+datePublished+"</td>"));
}

function changePage(pageNumber) {
    page = pageNumber;
    drawTable();
}